import React, { Component } from 'react';
import Todo from './Todo';
import './TodoList.scss';

class TodoList extends Component {
  renderTodos = todos => {
    return todos.map(todo => (
      // React requires us to give a unique 'key' property to items when we create dom elements with loops.
      // Here we use with 'id' property of each todo, but we could also just use the index of the loop.
      <li key={todo.id}>
        <Todo
          todo={todo}
          handleTodoStatusChange={this.props.handleTodoStatusChange}
        />
      </li>
    ));
  };

  render() {
    return (
      <div>
        {/* We move the logic for rendering this list of todos outside of the render function for readability's sake */}
        <ul>{this.renderTodos(this.props.todos)}</ul>
      </div>
    );
  }
}

export default TodoList;
