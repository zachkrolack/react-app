import React, { Component } from 'react';
import './Todo.scss';

class Todo extends Component {
  handleTodoStatusChange = () => {
    this.props.handleTodoStatusChange(this.props.todo.id);
  };

  render() {
    // Notice the use of 'className' instead of 'class'.
    // Since JSX is at its core just javascript, and class is keyword is js, we have to className to declare a css class.
    return (
      <div
        className={this.props.todo.completed ? 'done' : ''}
        onClick={this.handleTodoStatusChange}
      >
        {this.props.todo.text}
      </div>
    );
  }
}

export default Todo;
