import React, { Component } from 'react';
import './AddTodo.scss';

class AddTodo extends Component {
  handleChange = e => {
    // Call the parent components function that was passed to us via props with the input's value as a parameter.
    this.props.handleCurrentTodoTextChange(e.target.value);
  };

  handleSubmit = e => {
    // We have to call preventDefault to prevent the default form submission logic from happening.
    e.preventDefault();
    // Call the parent components function that was passed to us via props.
    this.props.handleAddTodo();
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="add a todo..."
            value={this.props.currentTodoText}
            onChange={this.handleChange}
          />
          <button>add</button>
        </form>
      </div>
    );
  }
}

export default AddTodo;
