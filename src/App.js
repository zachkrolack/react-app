import React, { Component } from 'react';
import AddTodo from './components/AddTodo';
import TodoList from './components/TodoList';
import './App.scss';

class App extends Component {
  constructor(props) {
    super(props);
    /*
      We declare the 'application' state in the highest level component so that we can pass the state to
      child components via props. This is a simple example application, so the state is not complicated
      and none of the child components need to have their own state. This, of course, is not always the
      case in more complex applications. The general idea is to think of the components as a tree where
      the state trickles down to the child components via props, so any value that needs to be saved in
      state needs to high enough up in the component tree to that it can react all of the child components
      that it needs to.
    */
    this.state = {
      todos: [
        { id: 0, text: 'feed the garden', completed: false },
        { id: 1, text: 'water the dog', completed: false }
      ],
      currentTodoId: 2,
      currentTodoText: ''
    };
  }

  /*
    I prefer using arrow functions to declare component member functions because they do not get
    their own 'this' scope. If you declare component functions as regular javascript functions,
    you will need to bind the scope of 'this' for each function to the 'this' scope of the ccmponent
    in the constructor. You will likely see this syntax in examples, so I thought it would be
    good to point out what it means and why I am not doing it here.
  */

  /**
   * Update currentTodoText with the given todoText.
   * @param todoText
   */
  handleCurrentTodoTextChange = todoText => {
    this.setState({
      currentTodoText: todoText
    });
  };

  /**
   * Add a todo to the list of todos.
   */
  handleAddTodo = () => {
    // Construct new todo object.
    const newTodo = {
      id: this.state.currentTodoId,
      text: this.state.currentTodoText,
      completed: false
    };

    this.setState(prevState => {
      return {
        todos: [newTodo, ...prevState.todos],
        currentTodoId: prevState.currentTodoId + 1,
        currentTodoText: ''
      };
    });
  };

  /**
   * Toggle the 'completed' field of a todo with the given id.
   * @param todoId
   */
  handleTodoStatusChange = todoId => {
    // Create a deep of copy of the todos property of the application state.
    // We do this because we do not want to mutate the state directly, but rather mutate a copy and replace the state.
    const todos = this.state.todos.map(todo => ({ ...todo }));
    // Find the todo with the given todoId.
    const todo = todos.find(todo => todo.id === todoId);
    // Toggle its 'completed' property.
    todo.completed = !todo.completed;
    this.setState({
      todos: todos
    });
  };

  render() {
    return (
      <div className="App">
        {/* We pass this component's functions to child components via props. */}
        {/* Also this is how you write a comment in the render function. */}
        <AddTodo
          currentTodoText={this.state.currentTodoText}
          handleCurrentTodoTextChange={this.handleCurrentTodoTextChange}
          handleAddTodo={this.handleAddTodo}
        />
        <TodoList
          todos={this.state.todos}
          handleTodoStatusChange={this.handleTodoStatusChange}
        />
      </div>
    );
  }
}

export default App;
